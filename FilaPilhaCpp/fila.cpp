#include <iostream>

using namespace std;

template <class A> 
class Fila {
protected:
   struct no {
      A cel;
      struct no *prox;
   };
   
   no *cab;
   no *ult;
   
public:
   Fila () {
      cab = new no;
      cab->prox = NULL;
      ult = cab;
   }

    // Insere fim
   void push (A n) {
      no *aux = new no;
      aux->cel = n;
      ult->prox = aux;
      ult = aux;
   }
   
   // Mostra início
   A front (void) {
      return cab->prox->cel;  
   }
   // Imprime
   void imprime () {
      no *aux = cab->prox;
      while (aux) {
         cout << aux->cel << " ";
         aux = aux->prox;      
      }
      cout << "\n";
   }
   
   // Está vazia
   bool empty () {
      return (cab->prox == NULL);
   }
   
   
   // Remove início
   void pop (void) {
      no *aux = cab->prox;
      cab->prox = cab->prox->prox;
      delete aux;
   }
};



