#include <iostream>

using namespace std;

template <class A> class Lista {
protected:
   struct no {
      A cel;
      struct no *prox;
   };
   
   no *cab;
   no *ult;
   
public:
   Lista () {
      cab = new no;
      cab->prox = NULL;
      ult = cab;
   }
   
   // Insere Início
   void insereIni (A n) {
      no *aux = new no;
      aux->cel = n;
      aux->prox = cab->prox;
      cab->prox = aux;
   }
   
   // Insere fim
   void insereFim (A n) {
      no *aux = new no;
      aux->cel = n;
      ult->prox = aux;
      ult = aux;
   }
   
   // Remove início
   void removeIni (void) {
      no *aux = cab->prox;
      cab->prox = cab->prox->prox;
      delete aux;
   }
   
   // Remove fim (interna)
   void removeF (no *c) { 
      if (c->prox->prox == NULL) { 
         no *aux = ult;
         aux->prox = NULL;
         ult = aux->prox;
         delete aux;
         return;     
      }      
      removeF (c->prox);   
   }
   
   // Remove fim (externa)
   void removeFim () {
      removeF(cab);
   }
   
   // Imprime
   void imprime () {
      no *aux = cab->prox;
      while (aux) {
         cout << aux->cel << " ";
         aux = aux->prox;      
      }
      cout << "\n";
   }
   
   // Busca
   int busca (A n) {
      no *aux = cab;
      while (aux) {
         if (aux->cel == n) return 1;
         aux = aux->prox;
      }
      return -1;
   }
   
};

