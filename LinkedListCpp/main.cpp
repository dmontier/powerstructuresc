#include <iostream>
#include "linked.cpp"

int main (int argc, char const* argv[]) {
   Lista <char> a;
   a.insereFim ('a');
   a.insereFim('b');
   a.insereFim('c');
   a.insereFim('d');
   a.imprime();
   a.insereIni('e');
   a.imprime();
   a.removeFim();
   a.imprime();
   cout << a.busca('c') << "\n";
   cout << a.busca('z') << "\n";
   return 0;
}
