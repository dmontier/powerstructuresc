#include"LinkedStack.h"
#include<stdio.h>
#include<stdlib.h>

LinkedStack* creatLinkedStack(){
    LinkedStack* li;
    li= (LinkedStack*)malloc(sizeof(LinkedStack));
    li->date=creatList();
    if(li==NULL){
        printf("Error");
        return NULL;
    }
    return li;
}

int isEmpty(LinkedStack* x){
    return emptyList(x->date); 
}
void pushLinked(LinkedStack* x, int item){
    addBeg(x->date,item);
}
int popLinked(LinkedStack* x){
   return (removeBeg(x->date));
}
void printStack(LinkedStack* x){
    printList(x->date);
}

void freeStack(LinkedStack* x){
    freeList(x->date);
    free(x);
    x=NULL;
}
