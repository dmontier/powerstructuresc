#include<stdio.h>
#include"CircleQueue.h"
#include<stdlib.h>

#define MAX (x->date->size)

ListCircle* creatCircleQue(int size){
    ListCircle* li;
    li=(ListCircle*) malloc(sizeof(ListCircle));
    li->date=creatList(size);
    if(li==NULL){
        printf("Error: List NULL");
        return NULL;
    }
    li->head=0;
    li->tail=0;
    li->sizeValid=0;
    return li;
}

int isEmpty(ListCircle* x){
    return (x->sizeValid==0);
}

void putItem(ListCircle* x, char item){
    if(x->sizeValid>=MAX){
        printf("The queue is full\n");
        return;
    }else{
        x->sizeValid++;
        x->date->item[x->tail] = item;
        x->tail = (x->tail+1)%MAX;
    }
    
}

int getItem(ListCircle* x){
    if(isEmpty(x)){
        printf("isempty\n");
        return -1;
    }
    else{
        int value=x->date->item[x->head];
        x->head=(x->head+1)%x->date->size;
        x->sizeValid--;
        return value;
    }
}

int printQueue(ListCircle* x){
    if(isEmpty(x)){
        printf("isempty\n");
        return 1;
    }
    int aux, aux1;
    aux  = x->head;
    aux1 = x->sizeValid;
    while(aux1>0){
        printf("#%d = %c\n", aux,x->date->item[aux]);
        aux=(aux+1)%x->date->size;
        aux1--;
    }  
    return 0;
}

void freeQueue(ListCircle* x){
    freelist(x->date);
    free(x);
    x=NULL;
}

int terms(ListCircle* x){
    /*if(x->tail<x->head)
        return (x->size - (x->head-x->tail));
    else
     return (x->tail - x->head);
     */
     return x->sizeValid;
}

void sizeUp(ListCircle* x,int size){
    char* up;
    int j=0;
    up=(char*) malloc(sizeof(char)*(size+x->date->size));
    int aux, aux1;
    aux  = x->head;
    aux1 = x->sizeValid;
    while(aux1>0){
        up[j]=x->date->item[aux];
        aux=(aux+1)%MAX;
        aux1--;
        j++;
    }  
    MAX+=size;
    x->head=0;
    x->tail=j;
    free(x->date->item);
    x->date->item=up;    
}
