/* By: Jeferson da Silva Juliani
 * Universidade Federal of Ceará
*/
#include"list.h"

typedef struct listCircle{
    int head,tail,sizeValid;
    List* date;
}ListCircle;

ListCircle* creatCircleQue(int size);
int isEmpty(ListCircle* x);
void putItem(ListCircle* x, char item);
int getItem(ListCircle* x);
int printQueue(ListCircle* x);
void freeQueue(ListCircle* x);
int terms(ListCircle* x);
void sizeUp(ListCircle* x,int size);
