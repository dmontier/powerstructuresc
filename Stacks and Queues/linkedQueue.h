/* By: Jeferson da Silva Juliani
 * Universidade Federal of Ceará
*/
#include"linkedlist.h"

typedef struct linkedQueue{
    Head* date;
}LinkedQueue;

LinkedQueue* creatQueue();
int isEmpty(LinkedQueue* x);
void putItem(LinkedQueue* x, int item);
int getItem(LinkedQueue* x);
int printQueue(LinkedQueue* x);
void freeQueue(LinkedQueue* x);
