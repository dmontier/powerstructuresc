/* By: Jeferson da Silva Juliani
 * Universidade Federal of Ceará
*/
#include"linkedlist.h"

typedef struct linkedStack{
    Head* date;
}LinkedStack;

LinkedStack* creatLinkedStack();
int isEmpty(LinkedStack* x);
void pushLinked(LinkedStack* x, int item);
int popLinked(LinkedStack* x);
void printStack(LinkedStack* x);
void freeStack(LinkedStack* x);
