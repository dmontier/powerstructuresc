/* By: Jeferson da Silva Juliani
 * Universidade Federal of Ceará
*/
#include"list.h"
typedef struct arrayStack{
    List* date;
}ArrayStack;

ArrayStack* creatStack(int size);
int isEmptyArray(ArrayStack* x);
void pushArray(ArrayStack* x, char item);
char popArray(ArrayStack* x);
void printArrayStack(ArrayStack* x);
void freeArrayStack(ArrayStack* x);
