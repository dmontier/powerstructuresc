/* By: Jeferson da Silva Juliani
 * Universidade Federal of Ceará
*/
#include<stdio.h>
#include <time.h>
#include<stdlib.h>
#define MAX 9
#define rep(b) for(int i=0;i<b;i++)

void swap(int[], int, int);
int partition(int[], int, int);
void quickSort(int A[], int lo, int hi);


void insertionSort(int *v, int size){
    int i, j, aux;
    for(i=1;i<size;i++){
        aux=v[i+1];
        for(j=i;(j>0) && (aux<v[j-1]);j--){
            v[j]= v[j-1];
        }
        v[j]=aux;
    }
}

void selectionSort(int *v, int size){ 
  int i, j, min, aux;
  for (i = 0; i < (size-1); i++){
     min = i;
     //SearchSmall
     for (j = (i+1); j < size; j++) {
       if(v[j] < v[min]) 
         min = j;
     }
     //swap
     if (i != min) {
       aux = v[i];
       v[i] = v[min];
       v[min] = aux;
     
     }
  }
}

void shellSort(int *vet, int size) {
    int i , j , value;
    int gap = 1;
    //gap 
    while(gap < size) {
        gap = 3*gap+1;
    }
    while ( gap > 1) {
        gap /= 3;
        for(i = gap; i < size; i++) {
            value = vet[i];
            j = i - gap;
            while (j >= 0 && value < vet[j]) {
                vet [j + gap] = vet[j];
                j -= gap;
            }
            vet [j + gap] = value;
        }
    }
}


void swap(int list[], int i, int j) {
    //swap list[i] and list[j]
    int hold = list[i];
    list[i] = list[j];
    list[j] = hold;
} //end swap

int partition(int A[], int lo, int hi) {
    //partition A[lo] to A[hi] using A[lo] as the pivot
    int pivot = A[lo];
    int lastSmall = lo;
    for (int h = lo + 1; h <= hi; h++)
        if (A[h] < pivot) {
            ++lastSmall;
        swap(A, lastSmall, h);
    }
    //end for
    swap(A, lo, lastSmall);
    return lastSmall; //return the division point
} //end partition1

void quickSort(int A[], int lo, int hi) {
    //sorts A[lo] to A[hi] in ascending order
    if (lo < hi) {
        int dp = partition(A, lo, hi);
        quickSort(A, lo, dp-1);
        quickSort(A, dp+1, hi);
    }
} //end quicksort

void merge(int A[], int lo, int mid, int hi) {
    int T[hi];
    int i = lo;
    int j = mid + 1;
    int k = lo;
    while (i <= mid || j <= hi) {
        if (i > mid) T[k++] = A[j++];
        else if (j > hi) T[k++] = A[i++];
        else if (A[i] < A[j]) T[k++] = A[i++];
        else T[k++] = A[j++];
    }
    for (j = lo; j <= hi; j++) A[j] = T[j];
}


void mergeSort(int A[], int lo, int hi) {
    if (lo < hi) { //list contains at least 2 elements
        int mid = (lo + hi) / 2; 
        mergeSort(A, lo, mid); 
        mergeSort(A, mid + 1, hi); //sort second half
        merge(A, lo, mid, hi); //merge sorted halves
    }
}
int getNextGap(int gap){
    // Shrink gap - Shrink factor
    gap = (gap*10)/13;
    if (gap < 1)
        return 1;
    return gap;
}
 

void combSort(int a[], int n){

    int gap = n;
    int swapped = 1;
 
    // Keep running while gap is more than 1 and last
    // iteration caused a swap
    while (gap != 1 || swapped){
        gap = getNextGap(gap);
        swapped = 0;
 
        // Compare all elements with current gap
        for (int i=0; i<n-gap; i++){
            if (a[i] > a[i+gap]){
                swap(a,i,i+gap);
                swapped = 1;
            }
        }
    }
}

int main(){
   int n;
    srand(1);                  
    printf("N= ");
    scanf("%d",&n);
    int *a,*b,*c,*d,*e,*f;
    a=(int*) malloc(sizeof(int)*n);
    b=(int*) malloc(sizeof(int)*n);
    c=(int*) malloc(sizeof(int)*n);
    d=(int*) malloc(sizeof(int)*n);
    e=(int*) malloc(sizeof(int)*n);
    f=(int*) malloc(sizeof(int)*n);
    rep(n){
        a[i]=b[i]=c[i]=d[i]=e[i]=rand()%100;
    }
   clock_t t_ini, t_fim;
   t_ini=clock();
   insertionSort(a,n);//insertion
   t_fim=clock();
   printf("\nInsertion = %.3f\n",((float) (t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   t_ini=clock();
   selectionSort(b,n);
   t_fim=clock();
   printf("\nSelection = %.3f\n",((float) (t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   t_ini=clock();
   shellSort(c,n);
   t_fim=clock();
   printf("\nShell = %.3f\n",((float) (t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   t_ini=clock();
   quickSort(d,0,n);
   t_fim=clock();
   printf("\nQuick = %.3f\n",((float)(t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   t_ini=clock();
   mergeSort(e,0,n);
   t_fim=clock();
   printf("\nMerge = %.3f\n",((float) (t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   t_ini=clock();
   combSort(f,n);
   t_fim=clock();
   printf("\nComb = %.3f\n",((float) (t_fim-t_ini)*1000/CLOCKS_PER_SEC));
   
   
   free(a);
   free(b);
   free(c);
   free(d);
   free(e);
   free(f);
   return 0;  
}
