#include<stdio.h>

int buscaBinRec(int arr[],int left,int right,int x){
 while(left<=right){
  int mid;
  mid=(left+right)/2;
  if(x==arr[mid])
   return mid;
  else if (x>arr[mid])
   return buscaBinRec(arr,mid+1,right,x);
  else
   return buscaBinRec(arr,left,mid-1,x);
 }
 return -1;
}

int main (){
 int vetor[4]={2,3,4,7},x;
 scanf("%d",&x);
 printf("%d\n",buscaBinRec(vetor,0,3,x));
 return 0;
}
