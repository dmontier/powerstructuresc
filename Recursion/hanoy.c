#include<stdio.h>
#include<time.h>
void hanoi(int n, char startPin, char endPin, char workPin) {
	if (n > 0) {
		hanoi(n - 1, startPin, workPin, endPin);
		printf("Move disk from %c to %c\n", startPin, endPin);
		hanoi(n - 1, workPin, endPin, startPin);
	}
}

int main (){
	int x;
	clock_t t; 
	scanf("%d",&x);
    t= clock(); 
 	hanoi(x, 'A', 'B', 'C');
 	t=clock()-t;
    printf ("tempo decorrido: %f\n",1000*((double)(t)/CLOCKS_PER_SEC));


 return 0;
}